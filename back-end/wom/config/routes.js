'use strict';


var home = require('../app/controllers/HomeController');
var place = require('../app/controllers/PlaceController');
var comment = require('../app/controllers/CommentController');
var user = require('../app/controllers/UserController');
var favorite = require('../app/controllers/FavoriteController');
var gcm = require('../app/controllers/GCMController');
var movie = require('../app/controllers/MovieController');
var coffee = require('../app/controllers/CoffeeController');
var mongoose = require('mongoose');
var Place = mongoose.model('Place');

module.exports = function(router, passport) {
  Place.count({}, function(err, count) {
    if (count <= 0) {
      place.loadInitialPlaces();
    }
  })

  router.get('/', home.index);
  router.get('/getAllPlaces',place.getAllPlaces);
  router.get('/getPlaceByLocation', place.getPlaceByLocation);
  router.get('/getCinemas', place.getCinemas);
  router.get('/getRestaurants',place.getRestaurants);
  router.get('/getWineBars',place.getWineBars);
  router.get('/getCafes',place.getCafes);
  router.get('/getPlaceOrderByDistance', place.getPlaceOrderByDistance);
  router.get('/getPlaceByQRCode', place.getPlaceByQRCode);
  router.get('/getPlaceByKey/:keywords', place.getPlaceByKey);
  router.get('/getPlaceById/:shop_id', place.getPlaceById);
  router.get('/getCommentsByShop/:shop_id', comment.getCommentsByShop);
  router.get('/getCommentsByUser/:user_id', comment.getCommentsByUser);
  router.get('/getPlaceOrderByRating', place.getPlaceOrderByRating);
  router.post('/saveComment', comment.saveComment);
  router.post('/socialLogin/:type', user.socialLogin);
  router.post('/favorite', favorite.addFavorite);
  router.get('/favorite/:user_id', favorite.getFavorite);
  router.put('/updateInfo/:place_id', place.updateInfo);
  router.put('/updateUserInfo/:user_id', user.updateInfo);
  router.put('/setAdmin/:email', user.setAdmin);
  router.get('/sendMessage', gcm.sendMsg);
  router.get('/getAllPlayingMovies', movie.getAllPlayingMovies);
  router.get('/getMovieInfo/:movie_id', movie.getMovieInfo);
  router.get('/getAllCoffees',coffee.getAllCoffees);
  router.get('/getCoffeesByUserId/:user_id',coffee.getCoffeesByUserId);
  router.post('/addCoffee',coffee.addCoffee);
  router.delete('/deleteCoffee/:coffee_id',coffee.deleteCoffee);
  router.post('/updateCoffee/:coffee_id',coffee.updateCoffee);
  router.put('/upvoteCoffee/:coffee_id',coffee.incrementUpvote);

  router.post('/signup', function*(next) {
    var ctx = this
    yield passport.authenticate('local-signup', function*(err, user, info) {
      if (err) throw err
      if (user === false) {
        ctx.status = 401 
        ctx.body = { success: false , status: 401 }
      } else {
        ctx.body = { success: true, user: user }
      }
    }).call(this, next)
  });
  router.post('/login', function*(next) {
    var ctx = this
    yield passport.authenticate('local-login', function*(err, user, info) {

      if (err) throw err
      if (user === false) {
        ctx.status = 401
        ctx.body = { success: false , status: 401}
      } else {
        ctx.body = { success: true, user: user }
      }
    }).call(this, next)
  });
};
