angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,
  localStorageService, ngProgressLite, $state, $cordovaBarcodeScanner) {
  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.user = localStorageService.get('user');
    if(localStorage.getItem("ranking") == undefined){
      localStorage.setItem("ranking",JSON.stringify({"id":"1","name":"Distance(Default)"}));

    }

    $scope.openSub = function(name) {
    $scope.submenu = true;
    $scope.selection = 'sub';
    }
    $scope.backToMain = function() {
    $scope.submenu = false;
    $scope.selection = 'main';
    }
    
  });

  $ionicModal.fromTemplateUrl('templates/basicsettings.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modalsettings) {
    $scope.modalsettings = modalsettings;
  });

  $scope.openModal = function() {
    $scope.modalsettings.show();
  };
  $scope.closeModal = function() {
    $scope.modalsettings.hide();
  };
  $scope.$on('$destroy', function() {
    $scope.modalsettings.remove();
  });

  $scope.data = {
    options: [
    { id: "1", name: "Distance(Default)"},
    { id: "2",  name: "Popularity"}
    ],
    selectedOption: { id: "1", name: "Distance(Default)"}
  };
  
  $scope.change_ranking = function(ranking){
     $scope.$broadcast("change_ranking_shoplist",ranking);
     localStorage.setItem("ranking",JSON.stringify(ranking));
     $scope.closeModal();
  }

  $scope.scan = function() {
    $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        console.log("BardCode", barcodeData)
        $http.get(Base_Url + "/getMovieInfo/" + barcodeData.text)
          .then(function(response) {
            localStorageService.set('movie', response.data);
            $state.go('app.movieDetails')
          });

      }, function(error) {

      });
  }
})

.controller('ShopCtrl', function($scope, $http, Base_Url, localStorageService,
  $ionicLoading, $compile, $ionicPopup, $ionicModal, $window, $firebaseArray) {

  $scope.shop = localStorageService.get('shop');
  $scope.user = localStorageService.get('user');
  var commentsRef = new Firebase("https://wit-wom.firebaseio.com/" + $scope.shop._id);
  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.comments = $firebaseArray(commentsRef);
    var gcm_id = localStorageService.get('gcm_id');
    if (gcm_id) {
      $http.put(Base_Url + "/updateUserInfo/" + $scope.user._id, { gcm_id: gcm_id }).then(function(response) {
        console.log(response);
      });
    }
  });

  $ionicModal.fromTemplateUrl('templates/postcomment.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  var rating;
  $scope.ratingsObject = {
    iconOn: 'ion-ios-star',
    iconOff: 'ion-ios-star-outline',
    iconOnColor: 'rgb(200, 200, 100)',
    iconOffColor: 'rgb(200, 100, 100)',
    rating: 0,
    minRating: 1,
    readOnly: true,
    callback: function(rating) {
      $scope.ratingsCallback(rating);
    }
  };

  $scope.ratingsCallback = function(rating) {
    this.rating = rating
  };

  $scope.post = function(comment) {
    var owner = null;
    if ($scope.shop.owner) {
      owner = $scope.shop.owner;
    }
    var comment_post_server = {
      'user': $scope.user._id,
      'place': $scope.shop._id,
      'rating': this.rating,
      'text': comment,
      'owner': owner
    }
    var comment_post_firebase = {
      'user': $scope.user.email,
      'place': $scope.shop._id,
      'rating': this.rating,
      'text': comment,
      'timestamp': Date.now()
    }
    $scope.comments.$add(comment_post_firebase).then(function() {
      var alertPopup = $ionicPopup.alert({
        title: 'WOM',
        template: 'Your comment has been posted!'
      });
      $scope.closeModal();
    });

    $http.post(Base_Url + "/saveComment", comment_post_server)
      .then(function(response) {});
  }

  function initialize() {
    var myLatlng = new google.maps.LatLng($scope.shop.latitude, $scope.shop.longitude);

    var mapOptions = {
      center: myLatlng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("shopmap"),
      mapOptions);

    var contentString = "<p>" + $scope.shop.name + "</p>";
    var compiled = $compile(contentString)($scope);

    var infowindow = new google.maps.InfoWindow({
      content: compiled[0]
    });

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'WOM'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
    });

    $scope.map = map;
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  initialize();

  $scope.add_fav = function(shop) {
    var data = {
      "place": shop._id,
      "user": $scope.user._id,
    }
    $http.post(Base_Url + "/favorite", data)
      .then(function(response) {
        if (response.data.msg == "added already") {
          var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'This shop has been saved already!'
          });
        } else {
          var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'This shop has been saved successfully!'
          });
        }
      });
  }
})

.controller('ShoplistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      var posOptions = { timeout: 10000, enableHighAccuracy: false };
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          var lat = position.coords.latitude
          var lng = position.coords.longitude
        }, function(err) {
        });

      if(JSON.parse(localStorage.getItem("ranking")) == null){
         $http.get(Base_Url + "/getPlaceOrderByDistance")
          .then(function(response) {
          $scope.shops = response.data;
        });
      }
      else if(JSON.parse(localStorage.getItem("ranking")).id == 1){
        $http.get(Base_Url + "/getPlaceOrderByDistance")
        .then(function(response) {
          $scope.shops = response.data;
        });
      }else if(JSON.parse(localStorage.getItem("ranking")).id == 2){
        $http.get(Base_Url + "/getPlaceOrderByRating")
        .then(function(response) {
          $scope.shops_popularity = response.data;
          var shops = [];
          $scope.shops_popularity.map(function(current_array){
            current_array.map(function(shop){
              shops.push(shop);
            })
          })
          $scope.shops = shops;
      });
      }
      
    });

    $scope.$on("change_ranking_shoplist",function(d,data){
      if(data.name == "Popularity"){
        localStorage.setItem("ranking",data);
        $http.get(Base_Url + "/getPlaceOrderByRating")
        .then(function(response) {
          $scope.shops_popularity = response.data;
          var shops = [];
          $scope.shops_popularity.map(function(current_array){
            current_array.map(function(shop){
              shops.push(shop);
            })
          })
          $scope.shops = shops;
      });
      }else{
        $http.get(Base_Url + "/getPlaceOrderByDistance")
        .then(function(response) {
          $scope.shops = response.data;
          console.log($scope.shops);
      });
      }
    })

    $scope.search = function(key){
     $http.get(Base_Url + "/getPlaceByKey/" + key)
      .then(function(response) {
        $scope.shops = response.data;
      });
    }

    $scope.clear = function(){
      $http.get(Base_Url + "/getPlaceOrderByDistance")
        .then(function(response) {
          $scope.shops = response.data;
      });
      document.getElementById('search_input').value = "";
    }

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single');
    }

  })
  .controller('CoffeeListCtrl',function($scope,$http,Base_Url,localStorageService,
    $ionicPopup,$ionicModal,$compile,$window){
    $scope.user = localStorageService.get('user');

    $scope.$on('$ionicView.beforeEnter',function(){ 
      $http.get(Base_Url + "/coffees")
        .then(function(response){
          console.log(response[0]);
          // var all_coffees = response.data;
          // localStorageService.set('coffees',all_coffees);
        })
    });

    var rating;
      $scope.ratingsObject = {
        iconOn: 'ion-ios-star',
        iconOff: 'ion-ios-star-outline',
        iconOnColor: 'rgb(200, 200, 100)',
        iconOffColor: 'rgb(200, 100, 100)',
        rating: 0,
        minRating: 1,
        readOnly: true,
        callback: function(rating) {
          $scope.ratingsCallback(rating);
        }
      };

      $scope.ratingsCallback = function(rating) {
        this.rating = rating;
      };

    $ionicModal.fromTemplateUrl('templates/addcoffee.html',{scope: $scope,animation: 'slide-in-up'})
      .then(function(modaladd) {
        $scope.modaladd = modaladd;
    });

    $scope.openAddModal = function() {
      $scope.modaladd.show();
    };
    $scope.closeAddModal = function() {
      $scope.modaladd.hide();
      setTimeout(function(){
        location.reload();
      },2500);
    };
    $scope.$on('$destroy', function() {
      $scope.modaladd.remove();
    });

    $scope.addcoffee = function(coffee_name,coffee_price,coffee_shop){
        var coffee_post = {
          'name': coffee_name,
          'shopname':coffee_shop.name,
          'user': $scope.user._id,
          'username': $scope.user.name,
          'price': coffee_price,
          'latitude': coffee_shop.latitude,
          'longitude': coffee_shop.longitude,
          'rating': this.rating,
      }

        $http.post(Base_Url + "/addCoffee", coffee_post)
          .then(function(response){
            if(response.data.status == 500){
              var alertPopup = $ionicPopup.alert({
              title: 'WOM',
              template: 'You have already added this coffee!'
              });
              $scope.closeAddModal();
            }else{
              var alertPopup = $ionicPopup.alert({
              title: 'WOM',
              template: 'Add successfully!'
              });
              $scope.closeAddModal();            
            }
          });
        }

    $ionicModal.fromTemplateUrl('templates/editcoffee.html', {
    scope: $scope,
    animation: 'slide-in-up'
    }).then(function(modaledit) {
      $scope.modaledit = modaledit;
    });

    $scope.openEditModal = function(coffee) {
      $scope.current_coffee = coffee;
      $scope.modaledit.show();
    };
    $scope.closeEditModal = function() {
      $scope.modaledit.hide();
      setTimeout(function(){
        location.reload();
      },2500);
    };
    $scope.$on('$destroy', function() {
      $scope.modaledit.remove();
    });

    $scope.deletecoffee = function(){
      $http.delete(Base_Url + "/deleteCoffee/" + $scope.current_coffee._id)
        .then(function(response){
          if(response.data.status == 500){
            var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'No coffee found!'
            });
            $scope.closeEditModal();
          }else{
            var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'Delete successfully!'
            });
            $scope.closeEditModal();
          }
        });
    }

    $scope.updatecoffee = function(coffee_name,coffee_price){        
      var coffee_update = {
        'name': coffee_name,
        'price': coffee_price,
        'rating': this.rating,
      }
      $http.post(Base_Url + "/updateCoffee/" + $scope.current_coffee._id, coffee_update)
        .then(function(response){
          if(response.data.status == 500){
            var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'No coffee founded!'
            });
            $scope.closeEditModal();
          }else{
            var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'Update successfully!'
            });
            $scope.closeEditModal();
          }
      });
    }

    function initialize() {
      var myLatlng = new google.maps.LatLng("52.263398", "-7.119431");
      var mapOptions = {
        center: myLatlng,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("coffeesmap"),
        mapOptions);

      var coffees = localStorageService.get('coffees');
      for(i in coffees){
        var coffee_location = { lat: Number(parseFloat(coffees[i].latitude)),
           lng: Number(parseFloat(coffees[i].longitude)) };
        var marker = new google.maps.Marker({
          position: coffee_location,
          map: map,
          title: 'WOM'
        });
        // marker.setMap(map);
        attachMessage(marker,i);
        // $scope.map = map;
      } // end of for loop

    function attachMessage(marker,i) {
      var contentString = "<p>" + coffees[i].name + "<br/>" + 
          "Shop: " + coffees[i].shopname + "<br/>" + 
          "Rating: " + coffees[i].rating + "🌟 <br/>" + 
          "Posted By: " + coffees[i].username + "<br/>" + 
          "</p>";
      var infowindow = new google.maps.InfoWindow({
        content: contentString,
       });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
      });
    }
      
    } // end of initialize()
    google.maps.event.addDomListener(window, 'load', initialize);
    initialize();
  })

  .controller('CinemalistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $http.get(Base_Url + "/getCinemas")
        .then(function(response) {
          $scope.cinemas = response.data;
        });
    });

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single');
    }
  })

  .controller('RestaurantlistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $http.get(Base_Url + "/getRestaurants")
        .then(function(response) {
          $scope.restaurants = response.data;
        });
    });

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single');
    }
  })

  .controller('WinebarlistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $http.get(Base_Url + "/getWineBars")
        .then(function(response) {
          $scope.winebars = response.data;
        });
    });

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single');
    }
  })

  .controller('CafelistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $http.get(Base_Url + "/getCafes")
        .then(function(response) {
          $scope.cafes = response.data;
        });
    });

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single');
    }
  })

  .controller('MovielistCtrl', function($scope, $http, Base_Url, $state,
    localStorageService, $cordovaGeolocation) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $http.get(Base_Url + "/getAllPlayingMovies")
        .then(function(response) {
          $scope.movies = response.data;
        });
    });

    $scope.go = function(movie) {
      localStorageService.set('movie', movie);
      $state.go('app.movieDetails');
    }
  })

  .controller('LogoutCtrl', function($scope, $http, Base_Url, $state, localStorageService) {
    $scope.$on('$ionicView.beforeEnter', function() {
      localStorageService.clearAll();
      $state.go('login');
    });
  })
  .controller('AdminCtrl', function($scope, $http, Base_Url, $state, localStorageService) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.setting = function() {
        $state.go("app.shopSetting")
      }
      $scope.userProfile = function() {
        console.log("userProfile");
      }
    });
  })
  .controller('AdminShopSettingCtrl', function($scope, $ionicPopup, $http, Base_Url, $state, localStorageService) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.user = localStorageService.get('user');

      $http.get(Base_Url + "/getPlaceById/" + $scope.user.ownShop)
        .then(function(response) {
          $scope.shop = response.data;
        });

    });

    $scope.setting = function(shop) {
      var data = {
        tel: shop.tel,
        website: shop.website,
        owner: $scope.user._id
      }
      $http.put(Base_Url + "/updateInfo/" + shop._id, data).then(function(response) {
        var alertPopup = $ionicPopup.alert({
          title: 'WOM',
          template: 'This shop details has been updated!'
        })
      });
    }
  })
  .controller('CommentCtrl', function($scope, $http, Base_Url, $state, localStorageService) {
    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.user = localStorageService.get('user');
      $http.get(Base_Url + "/getCommentsByUser/" + $scope.user._id)
        .then(function(response) {
          $scope.comments = response.data.reverse();
        });
    });

    $scope.go = function(shop) {
      localStorageService.set('shop', shop);
      $state.go('app.single')
    }
  })

.controller('LoginCtrl', function($ionicPopup, $scope, $state, $http, Base_Url, $ionicModal, localStorageService, $cordovaOauth) {
  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.user = localStorageService.get('user');
    if ($scope.user) {
      $state.go('app.shoplist');
    }
  });

  $scope.doLogin = function(user) {
    $http.post(Base_Url + "/login", user)
      .then(function(response) {
        console.log(response);
        if (response.data.status == 401) {
          var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'Email or Password Wrong!'
          });
        } else {
          localStorageService.set('user', response.data.user);
          $state.go('app.shoplist');
        }

      });
  }

  $scope.twitterLogin = function() {
    $cordovaOauth.twitter("WZ7gpm4SQCAMF85aq6kGtB06b", "cySZmZeXFg2ySrafa2KYVjw4vHGXv7HfLTcE0k3faUBUV7PRBk").then(function(result) {
      var user = {
        id: result.user_id,
        user_profile: result
      }
      $http.post(Base_Url + "/socialLogin/twitter", user)
        .then(function(response) {
          if (response.status != 500) {
            localStorageService.set('user', response.data);
            $state.go('app.shoplist');
          }
        });
    }, function(error) {
      console.log(error);
    });
  };

  $scope.facebookSignin = function() {
    $cordovaOauth.facebook("216963065306573", ["email"]).then(function(result) {
      var token = result.access_token;
      $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: token, fields: "id, name, picture", format: "json" } }).then(function(result) {
        var user = {
          id: result.data.id,
          user_profile: result.data
        }
        $http.post(Base_Url + "/socialLogin/facebook", user)
          .then(function(response) {

            if (response.status != 500) {
              localStorageService.set('user', response.data);
              $state.go('app.shoplist')
            }

          });
        $state.go('app.shoplist')
      }, function(error) {
        alert("There was a problem getting your profile.");
        console.log(error);
      });
    }, function(error) {
      alert("There was a problem signing in!");
      console.log(error);
    });
  };
})

.controller('SearchCtrl', function($scope, $state, $http, Base_Url, localStorageService) {
  $scope.data = { "shops": [], "search": '' }
  $scope.search = function(key) {
    $http.get(Base_Url + "/getPlaceByKey/" + key)
      .then(function(response) {
        $scope.data.shops = response.data;
      });
  }
  $scope.go = function(shop) {
    localStorageService.set('shop', shop);
    $state.go('app.single')
  }
  $scope.clear = function(){
      $scope.data.shops = null;
  document.getElementById('search_page').value = "";
  }

})

.controller('FavoritesCtrl', function($scope, $state, $http, Base_Url, localStorageService) {
  $scope.$on('$ionicView.beforeEnter', function() {
    $scope.user = localStorageService.get('user');

    $http.get(Base_Url + "/favorite/" + $scope.user._id)
      .then(function(response) {
        $scope.shops = response.data.reverse();
      });

  })


})

.controller('SignupCtrl', function($ionicPopup, $ionicModal, $scope, $state, $http, Base_Url, localStorageService) {
    $scope.avatars = [{avatar_id: 1},{avatar_id: 10},{avatar_id: 11},{avatar_id: 8}];
    
    $scope.choose_avatar = function(avatar){
      localStorage.setItem('avatar_id',avatar.avatar_id);
      $scope.closeModal();
      var alertPopup = $ionicPopup.alert({
      title: 'WOM',
      template: 'Successfully!'
      });
    };

    $ionicModal.fromTemplateUrl('templates/avatar.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

  $scope.doSignup = function(user) {
    var avatar_id = localStorage.getItem('avatar_id');
    if(user.password == user.password_confirmation){
      user.avatar_id = avatar_id;
      $http.post(Base_Url + "/signup", user)
        .then(function(response) {
        if (response.data.success == true) {
        localStorageService.set('user', response.data.user);
        $state.go('app.shoplist')
        }else{
          console.log(response);
        }
      });
    }else{
      var alertPopup = $ionicPopup.alert({
      title: 'WOM',
      template: 'Two passwords are not the same!'
      });
    }
  }

})

.controller('MovieFinderCtrl', function($scope, $state, $http, Base_Url, localStorageService, $cordovaBarcodeScanner) {
    $scope.scan = function() {
      $cordovaBarcodeScanner
        .scan()
        .then(function(barcodeData) {
          if(barcodeData){
            console.log("BardCode", barcodeData)
            $http.get(Base_Url + "/getMovieInfo/" + barcodeData.text)
            .then(function(response) {
              localStorageService.set('movie', response.data);
              $state.go('app.movieDetails')
            });
          }else{
            var alertPopup = $ionicPopup.alert({
            title: 'WOM',
            template: 'Movie Not Found!'
            });
          }
        }, function(error) {

        });
    }
  })

  .controller('MovieCommentCtrl', function($scope, $http, Base_Url, localStorageService,
  $ionicLoading, $compile, $ionicPopup, $ionicModal, $window, $firebaseArray) {
     $scope.movie = localStorageService.get('movie');
    var commentsRef = new Firebase("https://wit-wom.firebaseio.com/" + $scope.movie.id);
    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.user = localStorageService.get('user');
      $scope.comments = $firebaseArray(commentsRef);
    });
    $ionicModal.fromTemplateUrl('templates/postcomment.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };
    $scope.closeModal = function() {
      $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    var rating;
    $scope.ratingsObject = {
      iconOn: 'ion-ios-star',
      iconOff: 'ion-ios-star-outline',
      iconOnColor: 'rgb(200, 200, 100)',
      iconOffColor: 'rgb(200, 100, 100)',
      rating: 0,
      minRating: 1,
      readOnly: true,
      callback: function(rating) {
        $scope.ratingsCallback(rating);
      }
    };

    $scope.ratingsCallback = function(rating) {
      this.rating = rating
    };

    $scope.post = function(comment) {
      var comment_post_firebase = {
        'movie': $scope.movie.id,
        'rating': this.rating,
        'text': comment,
        'timestamp': Date.now()
      }
      $scope.comments.$add(comment_post_firebase).then(function() {
        var alertPopup = $ionicPopup.alert({
          title: 'WOM',
          template: 'Your comment has been posted!'
        });
        $scope.closeModal();
      });
    }

  })

.controller('DefaultSettingsCtrl', function($scope, $state, $http, Base_Url, localStorageService, $cordovaBarcodeScanner,$ionicPopup) {
    $scope.value = 2500;
    $scope.change_radius = function(radius){
      $scope.value = radius;
    }
    $scope.post_radius = function(radius){
      
      var alertPopup = $ionicPopup.alert({
          title: 'WOM',
          template: 'Distance radius changed successfully!'
        });
        $scope.closeModal();
        $state.go('app.shoplist')

    }    
  })
